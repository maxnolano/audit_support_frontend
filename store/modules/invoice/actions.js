export default {
    setHeadersInvoiceAction({ commit }, val) {
      commit("setHeadersInvoice", val);
    },
    setHeadersAccAction({ commit }, val) {
      commit("setHeadersAcc", val);
    }
  };
  