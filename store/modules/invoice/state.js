export default () => ({
headers_invoice: [
  { text: 'id', value: 'id', width: '20%'},
  { text: 'inv_name_sender', value: 'inv_name_sender', width: '65%'},
  { text: "", value: "controls", sortable: false },
],
headers_acc: [
  { text: 'id', value: 'id'},
  { text: 'cascade_type', value: 'cascade_type'},
  { text: 'inv_id', value: 'inv_id'},
  { text: 'inv_doc_num', value: 'inv_name_sender'},
  { text: 'inv_state_ref', value: 'inv_state_ref'},
  { text: 'inv_amnt_sender', value: 'inv_amnt_sender'},
  { text: 'inv_name_sender', value: 'inv_name_sender'},
  { text: 'inv_rnn_sender', value: 'inv_rnn_sender'},
  { text: 'inv_accnt_sender', value: 'inv_accnt_sender'},
  { text: 'inv_date_v_sender', value: 'inv_date_v_sender'},
  { text: 'inv_payment_purpose_sender', value: 'inv_payment_purpose_sender'},
  { text: 'inv_mfo_sender', value: 'inv_mfo_sender'},
  { text: 'inv_currency', value: 'inv_currency'},
  { text: "", value: "controls", sortable: false },
],
});
  