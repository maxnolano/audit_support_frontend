// import axios from "axios";
// import store from "@/store";
// import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_WARNING } from "@/consts";

// axios.defaults.baseURL = "/api/qaztrip/v2/";

// axios.interceptors.request.use(config => {
//   config.headers["Authorization"] = `Bearer ${store.state.auth.currentUser.access}`;

//   return config;
// });

// axios.interceptors.response.use(
//   res => res,
//   async error => {
//     let message = "Произошла ошибка";
//     let color = NOTIFICATION_TYPE_ERROR;

//     const loggedIn = store.getters["auth/loggedIn"];
//     const response = error.response;

//     const getAndSetErrorDetail = () => {
//       message = response?.data.detail;
//       color = NOTIFICATION_TYPE_WARNING;
//     };

//     if (response) {
//       switch (response.status) {
//         case 400:
//           getAndSetErrorDetail();
//           break;
//         case 401:
//           if (loggedIn) {
//             store.dispatch("auth/logout");
//             return Promise.reject(error);
//           } else getAndSetErrorDetail();
//           break;
//       }
//     }

//     store.dispatch("base/showNotification", { message, color });
//     return Promise.reject(error);
//   },
// );
